import json
import unittest
from montychat_sdk.secure_funcs import sign_payload, compare_signature


class TestCases(unittest.TestCase):

    def test_sign_and_verify_payload(self):
        # Test signing a message
        payload = {
            "sender_id": "32468109432",
            "message": "Hello, World!"
        }
        secret_key = "my_secret_key"
        payload_bytes = str(json.dumps(payload)).encode()
        signature = sign_payload(secret_key, payload_bytes)
        expected_signature = "fefb17cb3d1075de493c552f2d5f20b8875a354feae4aefdaaf47ad4d631ba71"
        verify_signature_result = compare_signature(signature, expected_signature)
        self.assertTrue(verify_signature_result)


if __name__ == "__main__":
    unittest.main()
