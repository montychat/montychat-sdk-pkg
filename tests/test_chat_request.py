import unittest

from montychat_sdk.chat_request import ChatRequest


class TestCases(unittest.TestCase):

    def test_simple_chat_request(self):
        request_data = {
            "next_action": "default_action",
            "sender_id": "32468109432",
            "tracker": {
                "sender_id": "32468109432",
                "slots": {
                    "session_started_metadata": None
                },
                "latest_message": {
                    "intent": {
                        "name": "greet",
                        "confidence": 1.0
                    },
                    "entities": [],
                    "text": "hello",
                    "message_id": "0b7eac9b9cc24f33a4b60b42af5c8f64",
                    "metadata": {},
                    "text_tokens": [
                        [0, 5]
                    ],
                    "intent_ranking": [
                        {
                            "name": "greet",
                            "confidence": 1.0
                        },
                        {
                            "name": "goodbye",
                            "confidence": 8.74939498629601e-09
                        },
                        {
                            "name": "back_to_bot",
                            "confidence": 6.154993847928836e-09
                        },
                        {
                            "name": "talk_to_human",
                            "confidence": 3.6336740283360314e-09
                        }
                    ],
                    "response_selector": {
                        "all_retrieval_intents": [],
                        "default": {
                            "response": {
                                "responses": None,
                                "confidence": 0.0,
                                "intent_response_key": None,
                                "utter_action": "utter_None"
                            },
                            "ranking": []
                        }
                    }
                },
                "latest_event_time": 1713885590.291635,
                "followup_action": None,
                "paused": False,
                "events": [
                    {
                        "event": "action",
                        "timestamp": 1713884878.009438,
                        "metadata": {
                            "assistant_id": "20240306-113825-drab-team",
                            "model_id": "ef33ce438e804167984eb89386d8cab2"
                        },
                        "name": "action_session_start",
                        "policy": None,
                        "confidence": 1.0,
                        "action_text": None,
                        "hide_rule_turn": False
                    },
                    {
                        "event": "session_started",
                        "timestamp": 1713884878.00944,
                        "metadata": {
                            "assistant_id": "20240306-113825-drab-team",
                            "model_id": "ef33ce438e804167984eb89386d8cab2"
                        }
                    },
                    {
                        "event": "action",
                        "timestamp": 1713884878.0094452,
                        "metadata": {
                            "assistant_id": "20240306-113825-drab-team",
                            "model_id": "ef33ce438e804167984eb89386d8cab2"
                        },
                        "name": "action_listen",
                        "policy": None,
                        "confidence": None,
                        "action_text": None,
                        "hide_rule_turn": False
                    }
                ],
                "latest_input_channel": "rest",
                "active_loop": {},
                "latest_action": {
                    "action_name": "action_unlikely_intent"
                },
                "latest_action_name": "action_unlikely_intent"
            },
            "version": "3.6.20",
            "domain": {
                "version": "3.1",
                "intents": ["back_to_bot", "goodbye", "greet", "talk_to_human"],
                "actions": ["default_action"],
                "session_config": {
                    "session_expiration_time": 60,
                    "carry_over_slots_to_new_session": True
                },
                "assistant_metadata": {
                    "assistant_id": "public"
                }
            }
        }
        request_headers = {
            "Content-Type": "application/json",
            "Content-Length": "81",
            "Connection": "Keep-Alive",
            "User-Agent": "Apache-HttpClient/4.5.14 (Java/17.0.9)",
            "x-montychat-signature": "1c53b9ac13edfb11f6b912de5d6e615b615c2a8824ad27cc5d1a019b7723827d"
        }

        chat_request = ChatRequest(request_data=request_data, assistant_secret="secret",
                                   request_headers=request_headers, verbose=True)
        self.assertEqual(chat_request.sender_id, "32468109432")
        self.assertEqual(chat_request.next_action, "default_action")
        self.assertEqual(chat_request.intent_name, "greet")
        self.assertEqual(chat_request.text, "hello")
        self.assertEqual(chat_request.response_message(),
                         {'responses': [{'text': None, 'buttons': [], 'custom': {'assistant_id': 'public'}}]})
        print(chat_request.verify_request_signature())
