import unittest
from unittest.mock import MagicMock, patch
import datetime
from montychat_sdk.funcs import error_handler, class_error_handler, stringify_model, generic_create_or_update
from montychat_sdk.funcs import format_phone_number


class SampleClass:
    def __init__(self, name, value):
        self.name = name
        self.value = value
        self._fields_ordered = ['name', 'value']

    def normal_method(self):
        return self.value

    def error_method(self):
        raise ValueError("An error!")

    def update_method(self, new_value):
        self.value = new_value


class TestCases(unittest.TestCase):

    @patch('montychat_sdk.funcs.logger')
    def test_error_handler(self, mocked_logger):
        result = error_handler(lambda x: x / 0)(1)
        mocked_logger.error.assert_called_once()
        self.assertIsNone(result)

    @patch('montychat_sdk.funcs.logger')
    def test_class_error_handler(self, mocked_logger):
        decorated_class = class_error_handler(SampleClass)
        instance = decorated_class("test", 10)
        result = instance.error_method()
        mocked_logger.error.assert_called_once()
        self.assertIsNone(result)

    def test_stringify_model(self):
        obj = SampleClass("test", 10)
        self.assertEqual(stringify_model(obj), "SampleClass(name=test, value=10)")

    @patch('montychat_sdk.funcs.get_current_time')
    def test_generic_create_or_update(self, mocked_datetime):
        mocked_datetime.return_value = datetime.datetime(2024, 1, 1)
        mocked_cls = MagicMock()
        mocked_obj = MagicMock()
        mocked_cls.objects.return_value.first.return_value = mocked_obj

        # Test update existing object
        generic_create_or_update(mocked_cls, {"name": "existing"}, value=20)
        mocked_obj.save.assert_called_once()
        self.assertEqual(mocked_obj.value, 20)

        # Test create new object
        mocked_cls.objects.return_value.first.return_value = None
        generic_create_or_update(mocked_cls, {"name": "new"}, name="new", value=30)
        mocked_cls.create.assert_called_with(name="new", value=30)

    def test_format_phone_number(self):
        self.assertEqual(format_phone_number("00123456789"), "+123456789",
                         "Should format '00123456789' as '+123456789'")
        self.assertEqual(format_phone_number("+123456789"), "+123456789", "Should keep '+123456789' unchanged")
        self.assertEqual(format_phone_number("123456789"), "+123456789", "Should prepend '+' to '123456789'")
        self.assertEqual(format_phone_number("+00123456789"), "+123456789",
                         "Should format '+00123456789' as '+123456789'")
        self.assertEqual(format_phone_number("00 + 123456789"), "+123456789",
                         "Should format '00 + 123456789' as '+123456789'")
        self.assertEqual(format_phone_number(""), "+")  # Assuming a '+' is still expected
        self.assertEqual(format_phone_number("+"), "+")
        self.assertEqual(format_phone_number("00"), "+")
        self.assertEqual(format_phone_number("!@#$%^&*()_+="), "+")  # Non-digit only


if __name__ == "__main__":
    unittest.main()
