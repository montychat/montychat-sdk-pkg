from setuptools import setup, find_packages
from version import __version__

setup(
    name='montychat_sdk',
    version=__version__,
    url='https://gitlab.com/montychat/montychat-sdk-pkg.git',
    author='Samo Masrie',
    author_email='samo.masrie@montymobile.com',
    description='This ',
    packages=find_packages(),
    install_requires=[
        'blinker==1.7.0',
        'certifi==2024.2.2',
        'charset-normalizer==3.3.2',
        'click==8.1.7',
        'Flask==3.0.3',
        'idna==3.7',
        'itsdangerous==2.2.0',
        'Jinja2==3.1.3',
        'MarkupSafe==2.1.5',
        'requests==2.31.0',
        'urllib3==2.2.1',
        'Werkzeug==3.0.2'
    ]

)
