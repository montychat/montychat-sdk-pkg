from typing import Text, List, Dict, Any, Optional
from flask import jsonify, make_response
import requests
import json
from montychat_sdk.funcs import class_error_handler
from montychat_sdk.secure_funcs import sign_payload, compare_signature


@class_error_handler
class ChatRequest:
    def __init__(self, request_data: dict, assistant_secret, request_headers=None, verbose=False):
        self.verbose = verbose
        self.assistant_secret = assistant_secret
        self.request_headers = request_headers
        self.request_data = request_data
        self.sender_id = request_data.get("sender_id", "")
        self.next_action = request_data.get("next_action", "")
        self.tracker = request_data.get("tracker", {})
        self.latest_message = self.tracker.get("latest_message", {})
        self.intent_name = self.latest_message.get("intent", {}).get("name", "")
        self.text = self.latest_message.get("text", "")
        self.domain = request_data.get("domain", {})
        self.events = self.tracker.get("events", [])
        self.total_events = len(self.events)

    def __str__(self):
        return (f"RasaAction: sender_id={self.sender_id}, "
                f"next_action={self.next_action}, "
                f"intent_name={self.intent_name}, "
                f"text={self.text}"
                f"total_events={self.total_events}")

    def print_or_log(self, msg: str):
        if self.verbose:
            print(msg)

    def verify_request_signature(self) -> bool:
        if self.request_headers is None:
            self.print_or_log("No request headers found. Request is not verified.")
            return False
        expected_signature = self.request_headers.get('x-montychat-signature')
        if expected_signature is None:
            self.print_or_log("No signature found in request headers. Request is not verified.")
            return False
        signature = sign_payload(secret_key=self.assistant_secret, payload_bytes=str(self.request_data).encode())
        self.print_or_log(f"Expected signature: {expected_signature}")
        self.print_or_log(f"Calculated signature: {signature}")

        return compare_signature(signature, expected_signature)

    def response_message(self, text: Optional[Text] = None,
                         custom_message: Optional[Dict[Text, Any]] = None,
                         buttons: Optional[List[Dict[Text, Any]]] = None):
        if custom_message is None:
            custom_message = {}
        custom_message["assistant_id"] = self.domain.get("assistant_metadata", {}).get("assistant_id", "")
        message = {
            "text": text,
            "buttons": buttons or [],
            "custom": custom_message or {},
        }
        response = {"responses": [message]}
        return response

    def make_response_flask(self, text: Optional[Text] = None,
                            custom_message: Optional[Dict[Text, Any]] = None,
                            buttons: Optional[List[Dict[Text, Any]]] = None,
                            verify_signature: bool = True,
                            add_signature: bool = True):
        if verify_signature and not self.verify_request_signature():
            return jsonify({"error": "Request signature verification failed"}), 401
        response_message = self.response_message(text=text, custom_message=custom_message, buttons=buttons)
        flask_response = make_response(jsonify(response_message))
        if add_signature:
            signature = sign_payload(secret_key=self.assistant_secret, payload_bytes=str(response_message).encode())
            flask_response.headers["x-montychat-signature"] = signature
        return flask_response

    def forward_request(self, url: str, add_signature: bool = True, do_verify_signature: bool = True):
        self.print_or_log(f"forward_request with url={url} ,self.request_data={self.request_data}")
        if add_signature:
            signature = sign_payload(secret_key=self.assistant_secret, payload_bytes=str(self.request_data).encode())
            headers = {
                'Content-Type': 'application/json',
                'x-montychat-signature': signature
            }
        else:
            headers = {'Content-Type': 'application/json'}
        request_response = requests.post(url, headers=headers, json=self.request_data)
        if do_verify_signature:
            expected_signature = request_response.headers.get('x-montychat-signature')
            signature = sign_payload(secret_key=self.assistant_secret,
                                     payload_bytes=str(self.response_message()).encode())
            if not compare_signature(signature, signature):
                return {"error": "Signature verification failed"}
        self.print_or_log(f"forward_request with url={url} and result={request_response.json()}, "
                          f"status_code={request_response.status_code}")
        return request_response.json()
