from montychat_sdk import logger
import datetime
import re
from functools import wraps


def get_current_time():
    return datetime.datetime.now()


def class_error_handler(cls=None, *, print_trace=False):
    if cls is None:
        return lambda cls: class_error_handler(cls, print_trace=print_trace)

    for name, method in vars(cls).items():
        if callable(method) and not name.startswith("__"):
            decorated_method = error_handler(method, cls.__name__, print_trace)
            setattr(cls, name, decorated_method)
    return cls


def error_handler(func, cls_name=None, print_trace=False):
    @wraps(func)
    def wrapper(*args, **kwargs):
        if print_trace:
            print(f"Calling: {cls_name}.{func.__name__}")
        try:
            return func(*args, **kwargs)
        except Exception as e:
            logger.error(f"An error occurred in {cls_name}.{func.__name__}: {e}")

    return wrapper


def stringify_model(instance):
    # Get the class name of the instance
    class_name = instance.__class__.__name__

    # Iterate over all fields of the instance
    fields_str = ', '.join([f"{field}={getattr(instance, field, None)}"
                            for field in instance._fields_ordered])

    return f"{class_name}({fields_str})"


def add_str_method(cls):
    cls.__str__ = lambda self: stringify_model(self)
    return cls


def generic_create_or_update(cls, query_criteria, **kwargs):
    obj = cls.objects(**query_criteria).first()
    if obj:
        for key, value in kwargs.items():
            if value and hasattr(obj, key):
                setattr(obj, key, value)
        obj.update_at = get_current_time()
        obj.save()
        return obj
    return cls.create(**kwargs)


def format_phone_number(phone_number):
    # Remove all spaces and non-digit characters except leading + or 00
    phone_number = re.sub(r'\D+', '', phone_number)

    if phone_number.startswith('00'):
        phone_number = phone_number[2:]

    # Prepend '+' to the resulting digits
    return '+' + phone_number
