import hashlib
import hmac

from montychat_sdk.funcs import error_handler


@error_handler
def sign_payload(secret_key: str, payload_bytes: bytes, digestmod=hashlib.sha256) -> str:
    # Calculate the HMAC SHA256 hash of the payload, using your app's secret key
    hmac_gen = hmac.new(key=secret_key.encode(), msg=payload_bytes, digestmod=digestmod)
    return hmac_gen.hexdigest()


@error_handler
def compare_signature(given_signature, expected_signature) -> bool:
    return given_signature == expected_signature
    # return hmac.compare_digest(given_signature, expected_signature)

