from flask import Flask, request, jsonify
import json
from montychat_sdk.chat_request import ChatRequest

app = Flask(__name__)


@app.route('/chat_request', methods=['POST'])
def handle_chat_request():
    print("chat_request: ", request.json)
    print("chat_request_headers: ", request.headers)
    print("chat_request_headers_type: ", type(request.headers))
    chat_request = ChatRequest(request_data=request.json, assistant_secret="testsecret123",
                               request_headers=request.headers, verbose=True)

    print(request.headers.get('connection'))
    print(chat_request)
    text = (f"Hello {chat_request.sender_id} from MontyChat!. "
            f"this is just a demo response from MontyChat.\n"
            f"you said: {chat_request.text}\n"
            f"intent_name: {chat_request.intent_name}\n"
            f"total_events: {chat_request.total_events}\n"
            f"next_action: {chat_request.next_action}\n")
    buttons = [{"title": "talk with agent", "payload": "talk wth agent"},
               {"title": "back to bot", "payload": "back to bot"}]
    flask_response = chat_request.make_response_flask(text=text, buttons=buttons, verify_signature=True,
                                                      add_signature=True)
    return flask_response


if __name__ == '__main__':
    app.run(port=5056, debug=True)
